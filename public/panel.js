require(['opshub/services', 'opshub/util'], function (services, util) {

    var service = services.selectedService();
 
    var reindexType = {
         FULL_REINDEX: 1,
         ONE_SITE_REINDEX: 2,
         MANY_SITE_REINDEX: 3
    };

    // The index to use for reindexing
    var indexName = {
         USER: "user",
         TEAM: "team"
    };

    function hideOneSiteReindexInput() {
       $('#cloud-id-input-div').hide()
    }
    
    function hideManySitesReindexInput() {
       $('#cloud-id-list-input-div').hide()     
    }

    function showOneSiteReindexInput() {
       $('#cloud-id-input-div').show()
    }

    function showManySitesReindexInput() {
       $('#cloud-id-list-input-div').show()
    }

    function getReindexType() {
       if ($('#full-reindex-radio').is(":checked")) {
         return reindexType.FULL_REINDEX
       } else if ($('#one-site-reindex-radio').is(":checked")) {
         return reindexType.ONE_SITE_REINDEX 
       } else if ($('#many-sites-reindex-radio').is(":checked")) {
         return reindexType.MANY_SITE_REINDEX
       }
    }

    function getIndexName() {
        if ($('#user-index-radio').is(":checked")) {
            return indexName.USER
        } else if ($('#team-index-radio').is(":checked")) {
            return indexName.TEAM
        } else {
            throw new Error("Unable to determine index name")
        }
    }

    $('input[type=radio][name=reindexing-type-radio]').change(function() {
        var tmp = getReindexType()
        if (tmp == reindexType.FULL_REINDEX) {
           hideOneSiteReindexInput() 
           hideManySitesReindexInput()
        } else if (tmp == reindexType.ONE_SITE_REINDEX) {
           hideManySitesReindexInput()
           showOneSiteReindexInput() 
        } else if (tmp == reindexType.MANY_SITE_REINDEX) {
           hideOneSiteReindexInput() 
           showManySitesReindexInput()
        }
    })

    function setHeaders(request) {
        request.setRequestHeader('content-type', 'application/json');
        request.setRequestHeader('accept', 'application/json');
    }
    
    function makeRequest(urlString, responseElement) {
        console.log(`Making request with url path: ${urlString}`)
       $.ajax({
            url: service.proxyUrl(urlString),
            type: 'PUT',
            beforeSend: setHeaders
        }).done(function(data) {
            data = JSON.stringify(data, null, 2);
            $(responseElement).append('Result for: ' + urlString + '<br>' + data + '<br>');
        }); 
    }

    $('#reindex-users-form').on('submit', function (e) {
        e.preventDefault();
        const cloudIdToReindex = $('#cloud-id-value').val();
        const cloudIdsToReindex = $('#cloud-id-list').val();
        const forceReindex = $('#force-check-box').is(":checked")
        var urlString = '/reindex/product/cpus'
        
        var tmp = getReindexType()
        var index = getIndexName()
        var urlParams = `force=${forceReindex}&index=${index}`

        if (tmp == reindexType.FULL_REINDEX) {
           makeRequest(`${urlString}/all?${urlParams}`, '#reindex-http-response')
        } else if(tmp == reindexType.ONE_SITE_REINDEX) {
           makeRequest(`${urlString}/cloud/${cloudIdToReindex}?${urlParams}`, '#reindex-http-response')
        } else if(tmp == reindexType.MANY_SITE_REINDEX) {
           var cloudIdList = cloudIdsToReindex.split('\n')
           $.each(cloudIdList, function(){
              var tmp = this.trim()
              var url = `${urlString}/cloud/${tmp}?${urlParams}`
              makeRequest(url, '#reindex-http-response');
           });  
        }
    });


    $('#delete-users-form').on('submit', function (e) {
        e.preventDefault();
        const userIdToDelete = $('#user-id-text').val();
        const urlString = `/document/product/cpus/user/${userIdToDelete}`

        $.ajax({
            url: service.proxyUrl(urlString),
            type: 'DELETE',
            beforeSend: setHeaders
        }).done(function(data) {
            data = JSON.stringify(data, null, 2);
            $('#delete-http-response').text('Result: ' + data);
        });
    });

    $('#delete-repo-form').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: service.proxyUrl(`/operations/delete-all-repositories`),
            type: 'DELETE',
            beforeSend: setHeaders
        }).done(function(data) {
            data = JSON.stringify(data, null, 2);
            $('#delete-repo-response').text('Result: ' + data);
        });
    });

    $('#delete-stale-child-form').on('submit', function (e) {
        e.preventDefault();
        const userId = $('#stale-user-id').val();
        if (!userId || userId.trim().length == 0) {
            throw new Error("User id can't be empty")
        }
        const urlTarget = `/operations/delete-all-children?userId=${userId}`;
        console.log(`Making DELETE request with url path: ${urlTarget}`);
        $.ajax({
            url: service.proxyUrl(urlTarget),
            type: 'DELETE',
            beforeSend: setHeaders
        }).done(function(data) {
            data = JSON.stringify(data, null, 2);
            $('#delete-stale-user-response').text('Result: ' + data);
        });
    });

    $('#delete-stale-all-child-form').on('submit', function (e) {
        e.preventDefault();
        const urlTarget = `/operations/delete-all-children`;
        console.log(`Making DELETE request with url path: ${urlTarget}`);
        $.ajax({
            url: service.proxyUrl(urlTarget),
            type: 'DELETE',
            beforeSend: setHeaders
        }).done(function(data) {
            data = JSON.stringify(data, null, 2);
            $('#delete-stale-all-response').text('Result: ' + data);
        });
    });
});
